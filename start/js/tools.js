var tools = {
  rootUrl: 'http://61.155.203.44:60161/Service.svc',
  /**
   * ajax数据请求
   * @param {*} url   地址 
   * @param {*} data  参数
   * @param {*} scb   成功回调
   * @param {*} ecb   失败回调
   */
  ajax: function(url, data, scb, ecb) {
    $.ajax({
      url: this.rootURL + url,
      type: 'POST',
      data: JSON.stringify(data),
      dataType:'JSON',
      headers: { "Content-Type": "application/json" },
      success: scb,
      error: ecb
    })
  },

  getA: function(url, scb, ecb) {
    $.ajax({
      url: url,
      type: 'GET',
      success: scb,
      error: ecb
    })
  },

  /**
   * 请求失败处理
   * @param {*} result 服务器返回的code
   * @param {*} attach 服务器返回的信息
   */
  msg: function(result, attach) {
    switch(result) {
      case -1:
        layer.msg("账号或密码错误，请重新输入");
        break;
      case 0:
        //默认不提示
        break;
      case -43:
      case -44:
        layer.msg("帐号退出，请重新登录");
        break;
      case -42:
        layer.msg("登录失效，请重新登录");
        break;
      case -41:
        location.reload();
        break;
      default:
        layer.msg(attach);
    }
  },

  /**
   * 获取url中的参数
   * @param {*} key 
   */
  getQueryString: function(key) {
    var url = window.location.search;
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    var result = url.substr(1).match(reg);
    return result ? decodeURIComponent(result[2]) : null;
  },

  /**
   * 删除数据
   * @param {*} d 对象，包含id及lasttime
   * @param {*} command 执行的table、view之类的
   * @param {*} callback 回调
   */
  dropDate: function(d, command, callback) {
    var commandLite = Object.assign({}, { commandKind: commandKinds.DROP }, command);
    var data = {
      verifyInfo: JSON.parse(localStorage.getItem("verifyInfo")),
      commandLite: commandLite,
      commandGuide: {
        id: d.id,
        lastTime: d.lastTime
      }
    }
  
    ajax(config.ExecuteCommand, data, function(res) {
      callback(res)
    }, function(err) {
      layer.msg('木有网络啦')
    })
  },

  /**
   * 根据id查询数据
   * @param {*} id 
   * @param {*} searchLite 查询条件
   * @param {*} callback 
   */
  readDataById: function(id, searchLite, callback) {
    var adata = {
      verifyInfo: JSON.parse(localStorage.getItem("verifyInfo")),
      searchLite: searchLite,
      id: id
    }
  
    ajax(config.ReadDataById, adata, function(res) {
      callback(res)
    }, function(err) {
      layer.msg('木有网络啦')
    })
  }
}
