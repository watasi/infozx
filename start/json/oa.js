var data = {
	banner: [
		{ id:1, img: "../start/images/oa/banner.jpg", url: "../start/images/oa/banner.jpg" }
	],
	list: [
		{
			style: "left",
			img: "../start/images/oa/8adce7f1-57c1-436a-bb11-717a1df58f4d.png",
			title: "软件介绍",
			// desc: "构建以用户为中心的统一办公门户，实现企业业务场景化，办公自动化，深度整合业务与管理系统，构建完善的一站式企业业务应用平台",
			descs: [
				"中系自动化办公系统（OA）是中系推出中小企业办公云服务，通过桌面PC或者移动客户端，可以快速的访问资源，处理公文，定制工作计划，主管可以向下安排任务，也可以验收工作成果。",
				"系统集成MSG及企业微博，有效的增进企业员工之间的交流，简单问题，可以用MSG传递给协助处理的人，复杂的问题，可以发布企业微博，让企业内部员工参与讨论，有效提升解决问题的效率，同时还增进员工只见的友谊。",
				"中系自动化办公系统（OA）还包含很多常用功能，这套系统已经在多家中小企业中运用，获得广泛的赞誉，是您不可多得的好助手。"
			]
		}, {
			style: "right",
			img: "../start/images/oa/aee9b404-73cc-4d8b-82e1-2bf6fd07941c.png",
			title: "即时通讯",
			// desc: "满足日常协同办公需求，提供公文、日程、会议、调查、车辆等几十个协同办公模块应用。",
			descs: [
				"企业内部沟通有效的工具，可以一对一，一对多两种方式发送信息，用户可以直接回复或者转发，还可以添加表情。",
				"报告是MSG的特殊形式，可以将多个人拉拢，形成一个小圈子，讨论一些事情，小圈子内的成员可以查看MSG的完整信息。",
				"MSG可以作为工作依据，减少了口误，推诿，狡辩等多种员工不良习惯，工作变得更加明确顺畅。"
			]
		}, {
			style: "left",
			img: "../start/images/oa/19e9a244-3c1c-4c15-889f-519155d3fbe4.png",
			title: "公文审批",
			// desc: "多移动入口(KK、钉钉等)，提供标准接入服务",
			descs: [
				"企业内部公文流转工具，按照严格的流程流转（发起>接请>决策>执行>回执），有条不紊，对于需要严谨处理的事件可以用公文审批的方式行报批。",
				"常用的公文包括：报请批示，行政报销，专案结算，财务审批，薪资审批等等，也可以按企业需求增加特殊的公文。",
				"可以针对不同的公文设置不同的阅读权限，以确保文件的安全性，可以公开的公文则可以进行公开，接受全公司职员的监督。也可以根据公文的需要，定制流程，比如接请后自己执行，或者直接转入执行。",
				"公文支持打印，在特殊情况下需要文本存档的时候，可以打印保存。"
			]
		}, {
			style: "right",
			img: "../start/images/oa/01bfcd01-1be2-475e-8dda-0f7a53de14c3.png",
			title: "计划总结",
			// desc: "多移动入口(KK、钉钉等)，提供标准接入服务",
			descs: [
				"计划与总结划分为年度目标，季度目标，月计划，周计划，将年度目标分解为季度目标，季度目标分解为月计划，月计划分解为周计划，周计划分解为日任务。",
				"系统对时间进行了管控，员工只有在规定的时间内填写计划与总结，有效避免了抄袭，早写，瞎写，盲写，促使员工养成一个良好的习惯。",
				"没有计划走不准，没有总结走不远"
			]
		}, {
			style: "left",
			img: "../start/images/oa/d0000569-5be9-45c0-8e2a-dcf1eec47a86.png",
			title: "企业微博",
			// desc: "多移动入口(KK、钉钉等)，提供标准接入服务",
			descs: [
				"企业微博提供了很多可能，比如一起讨论一个问题，发表一个心情，进行一次意向调查，反映一次工作改善意见，写上几句鼓励员工的话",
				"和普通微博一样，不经可以阅读微博内容，也能回复你的观点或意见，对感兴趣的人可以加关注，还可以顶他一下。",
				"和普通微博不一样，这里很纯净，只有公司的员工存在，对有效的微博还可以特殊管理，作为意见，建议分类，供主管参考。"
			]
		}, {
			style: "right",
			img: "../start/images/oa/33c07965-c1d2-40ed-b5c4-fe015456d352.png",
			title: "软件优势",
			// desc: "多移动入口(KK、钉钉等)，提供标准接入服务",
			descs: [
				"自主研发核心引擎采用先进的WCF通讯技术，安全可靠。",
				"与旗下其他系统数据互通，后期扩展可以无缝对接。",
				"支持权限管控（读写权限），流程管控（进退控制）。",
				"支持多平台客户端，PC，Android，IOS，WP8等系统。",
				"强大的研发团队，可以定制开发个性化需求。",
				"优秀的售后服务，保障您的后顾之忧。",
				"实时在线服务，快速解决问题。"
			]
		}, {
			style: "left",
			img: "../start/images/oa/d210d348-4413-4441-bd2e-f4034cdefb0a.png",
			title: "软件价值",
			// desc: "多移动入口(KK、钉钉等)，提供标准接入服务",
			descs: [
				"建立信息发布的平台,在内部建立一个有效的信息发布和交流的场所，使员工能够了解单位的发展动态。",
				"实现工作流程的自动化，就可以规范各项工作，提高单位协同工作的效率。",
				"实现知识管理的自动化,减少了很多培训环节。",
				"实现协同办公,支持多分支机构、跨地域的办公模式以及移动办公,提高整体的反应速度和决策能力。",
				"随着信息化的加强，传统的决策模式早已不能适应瞬息万变的信息社会，提高工作效率，加快企业内部信息沟通，领导作为快速，准确地决策，这都需要一个快速有效的办公自动化系统进行一系列有效之沟通。"
			]
		}
	]
}