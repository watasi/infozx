var data = {
	"banner": [
		{ id: 1, img: "../start/images/wx/banner.jpg", url: "../start/images/wx/banner.jpg" }
	],
	"why": {
		title: "企业为什么要做微信公众号开发",
		subtitle: "",
		list: [
			{
				id: 1,
				title: "移动互联网趋势",
				desc: "移动互联网用户已超传统互联网用户，成为目前的主流",
				icon: "../start/images/wx/we_img1.png"
			},
			{
				id: 2,
				title: "巨大流量入口",
				desc: "微信日平均用户达7亿，用户基数大，活跃度高",
				icon: "../start/images/wx/we_img2.png"
			},
			{
				id: 3,
				title: "强大的用户粘性和使用惯性",
				desc: "人人都在使用微信，其用户黏度没有任何一个产品可以比拟",
				icon: "../start/images/wx/we_img3.png"
			},
			{
				id: 4,
				title: "差异化服务",
				desc: "微信公众号开发定制，更有特色，更符合用户需求",
				icon: "../start/images/wx/we_img4.png"
			},
			{
				id: 5,
				title: "时尚的产品销售渠道",
				desc: "微商城开发，在线购买、支付、促销，互动最强的电商平台",
				icon: "../start/images/wx/we_img5.png"
			},
			{
				id: 6,
				title: "低成本营销",
				desc: "企业微信公众号是一次性投资，终身可以使用，同时有很多丰富的功能和应用帮企业推广和传播，物超所值",
				icon: "../start/images/wx/we_img7.png"
			}
		]
	},
	"ts": {
		title: "微信公众平台8大特色功能开发",
		subtitle: "",
		list: [
			{
				id: 1,
				icon: "../start/images/wx/fea_img2.png",
				title: "微信官网开发",
				background: "gw"
			}, {
				id: 2,
				icon: "../start/images/wx/fea_img3.png",
				title: "微信支付功能开发",
				background: "zf"
			}, {
				id: 3,
				icon: "../start/images/wx/fea_img6.png",
				title: "微信LBS定位功能开发",
				background: "dw"
			}, {
				id: 4,
				icon: "../start/images/wx/fea_img7.png",
				title: "微信会员功能开发",
				background: "hy"
			}, {
				id: 5,
				icon: "../start/images/wx/fea_img1.png",
				title: "微信H5页面制作",
				background: "ym"
			},
		]
	},
	"case": {
		title: "公众号开发案例",
		subtitle: "",
		list: [
			{
				id: 1,
				title: "邀请吧",
				logo: "../start/images/app/logo/yqb.png",
				desc: "邀请吧是一个为企业定制活动邀请的平台，可以组织活动，发布活动报名，优惠，产品介绍等内容。通过“邀请吧”APP、“邀请吧”微信公众号等社交平台分享传递给朋友及客户。",
				qrcode: "../start/images/wx/qrcode/yqb.dib"
			}, {
				id: 2,
				title: "保家适",
				logo: "../start/images/app/logo/bjs.png",
				desc: "保家适是一家集新房整装，二手房翻新，购建材，找设计，找工人，家居维护为一体的综合电商平台，为客户提供更快捷、更专业的装修购建材服务，真正的成为您的专属房管家。",
				qrcode: "../start/images/wx/qrcode/bjs.dib"
			}, {
				id: 3,
				title: "中系企管",
				logo: "../start/images/app/logo/qg.jpg",
				desc: "中系企管是中系信息科技专门打造的一款集信息技术与先进管理思想于一身，以系统化的管理思想，为企业员工及决策层提供决策手段的管理平台。",
				qrcode: "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1166872599,2586710492&fm=26&gp=0.jpg"
			}
		]
	}
}