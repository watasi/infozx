var data = {
	banner: [
		{ id: 1, img: "../start/images/erp/banner.jpg", url: "../start/images/erp/banner.jpg" }
	],
	function: {
		title: "产品功能",
		subtitle: "中系企管，是根据各行各业需求开发的一款在线的云端系统。将企业需要的所有功能整合到一个系统内，便于企业管理。",
		list: [
			{
				id: 1,
				icon: "fa-window-restore",
				title: "产品特色"
			}, {
				id: 2,
				icon: "fa-bank",
				title: "库存管理"
			}, {
				id: 3,
				icon: "fa-automobile",
				title: "生成/成本"
			}, {
				id: 4,
				icon: "fa-birthday-cake",
				title: "营业/销售"
			}, {
				id: 5,
				icon: "fa-cab",
				title: "订货/采购"
			}, {
				id: 6,
				icon: "fa-caret-square-o-up",
				title: "财务/会计"
			}, {
				id: 7,
				icon: "fa-coffee",
				title: "人事/薪资"
			}, {
				id: 8,
				icon: "fa-cutlery",
				title: "办公OA"
			}
		]
	},
	service: {
		title: "提供服务",
		subtitle: "",
		list: [
			{
				id: 1,
				icon: 'gx',
				title: "ERP更新"
			}, {
				id: 2,
				icon: 'jc',
				title: "视频教程"
			}, {
				id: 3,
				icon: 'erpgw',
				title: "ERP顾问"
			}, {
				id: 4,
				icon: 'ycxz',
				title: "远程协助"
			}, {
				id: 5,
				icon: 'zxzn',
				title: "在线指南"
			}
		]
	}
}