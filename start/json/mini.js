var data = {
  "banner": [
    { id: 1, img: '../start/images/mini/banner.jpg', url: '../start/images/mini/banner.jpg' }
  ],
  "advantage": {
    "title": "微信小程序优势",
    "subtitle": "共享微信全生态 触手可及微信9亿用户",
    "list": [
      {
        id: 1,
        icon: "fa-cloud-download",
        title: "无需下载",
        desc1: "小程序只需要扫一扫",
        desc2: "或者搜一下即可打开应用，方便快捷"
      },
      {
        id: 2,
        icon: "fa-mobile",
        title: "用完即走",
        desc1: "不再担心安装太多应用，",
        desc2: "小程序随时可用又无需安装卸载"
      },
      {
        id: 3,
        icon: "fa-folder-open",
        title: "降低门槛",
        desc1: "面对中小企业解决线上销售困境，",
        desc2: "降低电商创业门槛满足各项业务需求"
      },
      {
        id: 4,
        icon: "fa-lock",
        title: "安全稳定",
        desc1: "小程序商城依托微信平台",
        desc2: "资金流安全有保障接口更稳定"
      }
    ]
  },
  "entry": {
    "title": "众多流量入口增加品牌粘性",
    "subtitle": "",
    "list": [
      { id: 1, icon: "", title: "关联公众号" },
      { id: 2, icon: "", title: "附近门店" },
      { id: 3, icon: "", title: "好友推荐" },
      { id: 4, icon: "", title: "二维码" },
      { id: 5, icon: "", title: "历史搜索" },
      { id: 6, icon: "", title: "置顶小程序" },
      { id: 7, icon: "", title: "微信搜索" },
      { id: 8, icon: "", title: "消息通知" },
      { id: 9, icon: "", title: "自定义图标" },
      { id: 10, icon: "", title: "更多" },
    ]
  },
  "fit": {
    "title": "适用商户",
    "subtitle": "",
    "list": [
      { id: 1, title: "家居家纺", img: "../start/images/mini/sh/jjjf.png" },
      { id: 2, title: "医疗健康", img: "../start/images/mini/sh/yljk.png" },
      { id: 3, title: "美容养生", img: "../start/images/mini/sh/mrys.png" },
      { id: 4, title: "生鲜蔬果", img: "../start/images/mini/sh/sxgs.png" },
      { id: 5, title: "茶饮酒水", img: "../start/images/mini/sh/cyjs.png" },
      { id: 6, title: "门店", img: "../start/images/mini/sh/md.png" },
      { id: 7, title: "美妆", img: "../start/images/mini/sh/mz.png" },
      { id: 8, title: "礼品鲜花", img: "../start/images/mini/sh/lpxh.png" },
      { id: 9, title: "蛋糕烘培", img: "../start/images/mini/sh/dghp.png" },
      { id: 10, title: "服饰鞋帽", img: "../start/images/mini/sh/fsxm.png" },
      { id: 11, title: "汽车服务", img: "../start/images/mini/sh/qcfw.png" },
      { id: 12, title: "日用百货", img: "../start/images/mini/sh/rybh.png" },
      { id: 13, title: "教育培训", img: "../start/images/mini/sh/jypx.png" },
      { id: 14, title: "亲子母婴", img: "../start/images/mini/sh/qzmy.png" },
      { id: 15, title: "图书影像", img: "../start/images/mini/sh/tsyx.png" }
    ]
  },
  "support": {
    "title": "技术支持",
    "subtitle": "强大的研发实力，安全稳定可靠的系统支持",
    "list": [
      {
        "id": 1,
        "icon": "../start/images/mini/img1.png",
        "title": "安全稳定"
      },
      {
        "id": 2,
        "icon": "../start/images/mini/img2.png",
        "title": "智能高效"
      },
      {
        "id": 3,
        "icon": "../start/images/mini/img3.png",
        "title": "极致体验"
      },
    ]
  }
}