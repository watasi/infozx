var data = {
	banner: [
		{ id: 1, img: "../start/images/app/banner.jpg", url: "../start/images/app/banner.jpg" }
	],
	times: {
		title: "全方位移动营销时代，你准备好了吗？",
		subtitle: "十年巨变！移动互联网已颠覆我们的时代，商业、社交、视频、新闻、工具等领域，移动应用渗透率已超过80%，在团购、旅游和零售业，移动端收入规模已经超越PC端，手机APP已经成为企业不可或缺的线上阵地。"
	},
	case: {
		title: "行业案例",
		subtitle: "",
		list: [
			{
				id: 1,
				title: "邀请吧",
				logo: "../start/images/app/logo/yqb.png",
				desc: "邀请吧是一个为企业定制活动邀请的平台，可以组织活动，发布活动报名，优惠，产品介绍等内容。通过“邀请吧”APP、“邀请吧”微信公众号等社交平台分享传递给朋友及客户。",
				qrcode: "../start/images/wx/qrcode/yqb.dib"
			}, {
				id: 2,
				title: "保家适",
				logo: "../start/images/app/logo/bjs.png",
				desc: "保家适是一家集新房整装，二手房翻新，购建材，找设计，找工人，家居维护为一体的综合电商平台，为客户提供更快捷、更专业的装修购建材服务，真正的成为您的专属房管家。",
				qrcode: "../start/images/wx/qrcode/bjs.dib"
			}, {
				id: 3,
				title: "中系企管",
				logo: "../start/images/app/logo/qg.jpg",
				desc: "中系企管是中系信息科技专门打造的一款集信息技术与先进管理思想于一身，以系统化的管理思想，为企业员工及决策层提供决策手段的管理平台。",
				qrcode: "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1166872599,2586710492&fm=26&gp=0.jpg"
			}
		]
	},
	plat: {
		title: "跨平台",
		subtitle: "一套代码，多端运行",
		image: "../start/images/app/gwc.png"
	},
	ys: {
		title: "我们的优势",
		subtitle: "",
		list: [
			{
				id: 1,
				title: "绝佳的用户体验",
				icon: "../start/images/app/app_feature_1.png"
			}, {
				id: 2,
				title: "行业技术领先",
				icon: "../start/images/app/app_feature_2.png"
			}, {
				id: 3,
				title: "配套服务齐全",
				icon: "../start/images/app/app_feature_3.png"
			}
		]
	}
}