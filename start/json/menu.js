{
  "code": 0,
  "msg": "",
  "data": [
    {
      "title": "首页",
      "url": "#/",
      "flag": ""
    },
    {
      "title": "产品方案",
      "flag": "product",
      "children": [{
        "title": "ERP",
        "url": "#/product/erp/"
      }, {
        "title": "CRM",
        "url": "#/product/crm/"
      }, {
        "title": "OA",
        "url": "#/product/oa/"
      }, {
        "title": "HR",
        "url": "#/product/hr/"
      }]
    },
    {
      "title": "关于我们",
      "flag": "about",
      "children": [{
        "title": "中系简介",
        "url": "#/about/introduce/"
      }, {
        "title": "行业优势",
        "url": "#/about/advantage/"
      }, {
        "title": "荣誉资质",
        "url": "#/about/honor/"
      }]
    }
  ],
  "qrcode": "../start/images/wx/qrcode/zxkj.dib"
}