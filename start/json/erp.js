var data = {
	banner: [
		{ id:1, img: "../start/images/erp/banner.jpg", url: "../start/images/erp/banner.jpg" }
	],
	list: [
		{
			style: "left",
			img: "../start/images/erp/e52041fd-8578-4710-bb9e-f15e2836e3ce.jpg",
			title: "软件结构",
			descs: [
				"SYSTEM：底层系统 必要支持，如通讯协议，数据结构，基础服务，数据加密",
				"CLOUD：云数据存储 跨平台基础，提供安全的数据交互与持久化存储",
				"HR：人力资源管理系统",
				"OA：自动化办公系统",
				"CRM：客户关系管理系统",
				"PSS：进销存管理系统",
				"FD：财务管理系统",
				"EAM：企业资产管理系统"
			]
		}, {
			style: "right",
			img: "../start/images/erp/22be6885-eda8-4080-a8de-4cabe002614a.png",
			title: "软件介绍",
			descs: [
				"企业迫切期望拥有一套能够适应网络时代，快速获取有效信息，支持企业战略升级，业务灵活变化的ERP系统。",
				"中系企业资源管理系统（ERP），是以网络通讯为基础，敏捷办公为理念的企业资源管理软件，通过定制开发，满足大中小企业的个性化需求。",
				"中系企业资源管理系统（ERP），以人力，物力，财力为核心，全面管控企业必须的工作计划，业务策略，相关数据，作业流程，公文审批，企业文化等各个方面。",
				"中系企业资源管理系统（ERP），以先进的WCF作为通讯基础，采用实时推送信息，多平台客户端操作，突破传统束缚，实现随时随地办公，省时，省心，省力。",
				"中系企业资源管理系统（ERP），软件的管理功能由各大模块组成，具备拆合能力，根据企业现阶段的需求，合理选择功能模块，可以节省支出，又可以在后期扩展，增强功能。"
			]
		}, {
			style: "left",
			img: "../start/images/erp/aeb10a79-da50-4a5b-9a51-919d9910a9e1.jpg",
			title: "软件特点",
			descs: [
				"企业内部管理所需的业务应用系统，主要是指财务、物流、人力资源等核心模块。",
				"物流管理系统采用了制造业的MRP管理思想；FMIS有效地实现可预算管理、业务评估、管理会计、ABC成本归集方法等现代基本财务管理方法；人力资源管理系统在组织机构设计、岗位管理、薪酬体系以及人力资源开发等方面同样集成了先进的理念。",
				"中系ERP软件是一个在全公司范围内应用的、高度集成的系统。数据在各业务系统之间高度共享，所有源数据只需在某一个系统中输入一次，保证了数据的一致性。",
				"对公司内部业务流程和管理过程进行了优化，主要的业务流程实现了自动化。",
				"采用了计算机最新的主流技术和体系结构：B/S、INTERNET体系结构，WINDOWS界面。在能通信的地方都可以方便地接入到系统中来。",
				"集成性、先进性、统一性、完整性、开放性。"
			]
		},  {
			style: "right",
			img: "../start/images/erp/99b08608-b0f6-42b0-9275-14678ff0743f.jpg",
			title: "软件优势",
			descs: [
				"行业定制&nbsp;&nbsp;&nbsp;&nbsp;规范流程&nbsp;&nbsp;&nbsp;&nbsp;管理思想&nbsp;&nbsp;&nbsp;&nbsp;专业服务&nbsp;&nbsp;&nbsp;&nbsp;权限配置",
				"桌面平台&nbsp;&nbsp;&nbsp;&nbsp;移动平台&nbsp;&nbsp;&nbsp;&nbsp;实时提醒&nbsp;&nbsp;&nbsp;&nbsp;自动备份&nbsp;&nbsp;&nbsp;&nbsp;数据加密",
				"公文管理&nbsp;&nbsp;&nbsp;&nbsp;实时消息&nbsp;&nbsp;&nbsp;&nbsp;企业微博&nbsp;&nbsp;&nbsp;&nbsp;企业文化&nbsp;&nbsp;&nbsp;&nbsp;动态题库",
				"扩展简单&nbsp;&nbsp;&nbsp;&nbsp;携带方便&nbsp;&nbsp;&nbsp;&nbsp;云端数据&nbsp;&nbsp;&nbsp;&nbsp;随时随地&nbsp;&nbsp;&nbsp;&nbsp;整合通讯",
				"界面简洁&nbsp;&nbsp;&nbsp;&nbsp;功能全面&nbsp;&nbsp;&nbsp;&nbsp;操作简单&nbsp;&nbsp;&nbsp;&nbsp;上手容易&nbsp;&nbsp;&nbsp;&nbsp;系统培训",
				"全程实施&nbsp;&nbsp;&nbsp;&nbsp;精干团队&nbsp;&nbsp;&nbsp;&nbsp;质量承诺&nbsp;&nbsp;&nbsp;&nbsp;在线服务&nbsp;&nbsp;&nbsp;&nbsp;专员维护"
			]
		}
	]
}