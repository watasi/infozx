var data = {
    "banner": [
      { "id": 1, "img": "../start/images/index/banner/banner_1.jpg" },
      { "id": 2, "img": "../start/images/index/banner/banner_2.jpg" }
    ],
    "support": {
      "title": "技术支持",
      "subtitle": "强大的研发实力，安全稳定可靠的系统支持",
      "list": [
        {
          "id": 1,
          "icon": "../start/images/mini/img1.png",
          "title": "安全稳定"
        },
        {
          "id": 2,
          "icon": "../start/images/mini/img2.png",
          "title": "智能高效"
        },
        {
          "id": 3,
          "icon": "../start/images/mini/img3.png",
          "title": "极致体验"
        },
      ]
    },
    "miniPro": [
      {
        "id": 1,
        "img": "../start/images/index/f1_business@2x.png",
        "url": "../start/images/index/f1_business@2x.png",
        "title": "微商城",
        "subtitle": "小程序+公众号一体化微信商城",
        "desc": "轻松拥有在线商城，几十种促销工具支持，深耕会员精准营销，为每个企业的成功保驾护航。",
        "applys": "食品、服饰、美妆、母婴、家居、箱包等全行业"
      },
      {
        "id": 2,
        "img": "../start/images/index/f1_zhan@2x.png",
        "url": "../start/images/index/f1_zhan@2x.png",
        "title": "微站",
        "subtitle": "3分钟可视化打造自己的小程序",
        "desc": "丰富的模板、组件库满足您的个性化需求，完美兼容小程序和公众号。",
        "applys": "企业官网、婚纱、汽车、房产、自媒体等"
      },
      {
        "id": 3,
        "img": "../start/images/index/f1_kld@2x.png",
        "url": "../start/images/index/f1_kld@2x.png",
        "title": "客来店",
        "subtitle": "助力线下门店抢占小程序流量红利",
        "desc": "快速实现线上销售！营销活动多，拉客效果好，带动新老客到店消费。",
        "applys": "教育培训、政府机构、运动健身、生活服务、房产"
      },
      {
        "id": 4,
        "img": "../start/images/index/f1_xst@2x.png",
        "url": "../start/images/index/f1_xst@2x.png",
        "title": "销售推",
        "subtitle": "助力销售快速获客的解决方案",
        "desc": "帮助企业提升销售能力、洞察客户行为、挖掘意向客户，实现企业全员流量变现。",
        "applys": "名片与产品分享、社交化营销、销售获客、客户转化等"
      }
    ],
    "products": [
      {
        "id": 1,
        "img": "../start/images/index/pros/erp.png",
        "url": "#/product/erp/",
        "title": "ERP",
        "desc": "企业资源计划（enterprise resource planning，缩写ERP），或译企业资源规划，是一个由美国著名的高德纳咨询公司于1990年提出的企业管理概念。企业资源计划最初被定义为应用软件，但迅速为全世界商业企业所接受。"
      },
      {
        "id": 2,
        "img": "../start/images/index/pros/app.png",
        "url": "#/app/",
        "title": "APP",
        "desc": "移动应用程序（英语：mobile application，简称mobile app、apps），或称手机应用程序、移动应用程序、移动应用、手机app等，是指设计给智能手机、平板电脑或其他移动设备运行的一种应用程序。"
      },
      {
        "id": 3,
        "img": "../start/images/index/pros/cx.png",
        "url": "#/mini/",
        "title": "小程序",
        "desc": "微信小程序是腾讯于2017年1月9日推出的一种不需要下载安装即可在微信平台上使用的应用，主要提供给企业、政府、媒体、其他组织或个人的开发者在微信平台上提供服务。"
      },
      {
        "id": 4,
        "img": "../start/images/index/pros/gzh.png",
        "url": "#/wx/",
        "title": "公众号",
        "desc": "微信支持希望注册为公众号的用户将内容推送给订阅户，与订阅户进行互动，给他们提供服务。目前公众号分三种：服务号、订阅号和企业号。一旦用户以个人或组织的名义设立一种类型的账户，就不能更该账户的类型。"
      }
    ],
    "aibou": [
      { "id": 1, "img": "../start/images/index/aibou/1.png", "url": "../start/images/index/aibou/1.png" },
      { "id": 2, "img": "../start/images/index/aibou/2.png", "url": "../start/images/index/aibou/2.png" },
      { "id": 3, "img": "../start/images/index/aibou/3.png", "url": "../start/images/index/aibou/3.png" },
      { "id": 4, "img": "../start/images/index/aibou/4.png", "url": "../start/images/index/aibou/4.png" },
      { "id": 5, "img": "../start/images/index/aibou/5.png", "url": "../start/images/index/aibou/5.png" },
      { "id": 6, "img": "../start/images/index/aibou/6.png", "url": "../start/images/index/aibou/6.png" },
    ]
}