var data = {
	banner: [
		{ id: 1, img: "../start/images/honor/banner.jpg", url: "../start/images/honor/banner.jpg" }
	],
	honor: {
		title: '资质荣誉',
		subtitle: '自中系信息科技有限公司成立以来，以其优秀的现代管理意识、高水准的服务体系，充分利用自身技术优势，为企业的品牌建设，产品推广提供了更好的帮助与支持；为了保证更高标准的服务质量，我们不断提高自身素质及工作效率，以优异的品质，快速精良的创意制作，本着一切以顾客为本，一切以质量服务为先的原则，以只做最先进的，只提供最好的为原，通过全体员工的不懈努力，赢得了社会各界的公认，获得了一个接着一个的优异奖项。',
		list: [{
			img: '../start/images/honor/6a7c8c45-4fc5-4eda-a5cf-f464c4f5839b.png',
			code: '奖项编号：KJCG130463',
			desc: '中系在2013年借由ERP软件协助苏州安得装饰公司获得全国建筑装饰行业科技成果奖。'
		}, {
			img: '../start/images/honor/3ad6f3a5-c77d-41d7-9d44-b3213a704da2.png',
			code: '奖项编号：IIBDI-41-2010',
			desc: '中系在2010年借由ERP系统与OA平台协助苏州安得装饰公司获得全国建筑装饰行业科技成果奖。'
		}, {
			img: '../start/images/honor/3a72c35e-cb75-4965-a974-39ecc4246bba.png',
			code: '奖项编号：IIBDI-16-2009',
			desc: '中系在2009年借由OA办公软件协助苏州安得装饰公司获得全国建筑装饰行业科技成果奖。'
		}]
	}
}